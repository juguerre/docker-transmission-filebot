# Sonarr - Transmission - filebot - Plex: Gestión de descarga y visualización de series torrent

[![pipeline status](https://gitlab.com/juguerre/docker-transmission-filebot/badges/master/pipeline.svg)](https://gitlab.com/juguerre/docker-transmission-filebot/commits/master)

## Intro
Este proyecto implementa una solución con un conjunto coordinado de
contenedores via docker-compose/Swarm-stack y scripts para gestionar la descarga y visualización
 de torrents.
- [Plex Media Server](https://www.plex.tv/es/): Organizador y reproductor
 Este contenedor se basa en la imagen de [`linuxserver/plex`](https://hub.docker.com/r/linuxserver/plex/)  
- [Sonarr](): Gestión de descarga de torrents especializado en series.
Este contenedor se basa en la imagen de [`linuxserver/sonarr`](https://hub.docker.com/r/linuxserver/sonarr/)
- [Radarr](): Gestión de descarga de torrents especializado en películas.
Este contenedor se basa en la imagen de [`linuxserver/radarr`](https://hub.docker.com/r/linuxserver/radarr/)
- [Lidarr](): Gestión de descarga de torrents especializado en música.
Este contenedor se basa en la imagen de [`linuxserver/lidarr`](https://hub.docker.com/r/linuxserver/lidarr/)
- [Organizr](): Utilidad para unificar el acceso a los distintos servicios del
stack/compose.
Se basa en la imagen de [`lsiocommunity/organizr`](https://hub.docker.com/r/lsiocommunity/organizr/)
- [Transmission](): Cliente de descarga torrent. En esta solución es el
cliente empleado por Sonarr para la descarga. Este contenedor se basa
en la imagen de [`linuxserver/transmission`](https://hub.docker.com/r/linuxserver/transmission/)
- [filebot](https://www.filebot.net): Utilidades Groovy para el
renombrado de multimedia y la descarga de subtitulos. Se ejecutan
periodicamente script para la descarga y gestión de subtitulos. Este
contenedor se basa en la imagen oficial de [`groovy:alpine`](https://hub.docker.com/_/groovy/)

...

## Características de la solución (TODO)

## Arranque rápido

- Revisa docker-compose.yml para sustituir la variables adecuadas:
    - Ubicación de los directorios de descarga de torrents y de videos
- Verifica que no hay conflicto de puertos con los que están definidos
en docker-compose.yml
- Crea el volumen externo _plex_data_ que luego se referencia en la
configuración: `docker volume create plex_data`
- Crear la red externa _traefik-network_ que luego se referencia en la configuración:
 `docker network create traefik-network`
- Se requiere una cuenta en https://www.opensubtitles.org/es y configurar
el usuario y password en la variables de entorno de `docker-compose.yml`


## Configuración


### Puertos de servicio
La configuración del docker-compose expone los puertos estándar de los
servicios Plex y Transmission-daemon con lo que **es aconsejable parar los
servicios de Plex y Transmission-daemon en el host si estos están en
marcha.**

### Volumenes a definir y mapear

- **Directorio de "downloads" principal**: Se trata de la ruta principal
donde se inician las descargas (descargas temporales). Un ejemplo podría
 ser `/home/<user>/downloads/`
- **Directorio de "downloads" para descargas finalizadas de series**:
Se trata de la ruta donde se almacenan las descargas finalizadas antes
de ser procesadas. Un ejemplo podría ser `/home/<user>/downloads/complete/series`
- **Directorio de repositorio final de series**: Se trata de la ruta
al directorio donde se ubican las series para ser visualizadas. Es decir,
 es el directorio que **Plex** monitorizará para series. En este
directorio los videos ya están procesados y renombrados. También es donde
se ubicarán los subtitulos de asociados a cada video. Un ejemplo podría
 ser `/home/<user>/videos/series`. Por otro lado bajo este directorio
 la estructura podría ser por ejemplo:
    - `/home/<user>/videos/series/serie1/`
    - `/home/<user>/videos/series/serie2/`
    - etc...

> Nota: Adicionalmente en el servicio de Plex se pueden mapear la ubicación de
otro tipo de media: peliculas, música, etc.

Con objetivo de persistir datos de configuración se configura un volumen
externo para poder ser reutilizado.
Para crear el nuevo volumen requerido:

```shell
docker volume create plex_data
```
### Red traefik
Esta configuración docker-compose está preparada para ejecutarse junto con
un reverse proxy _traefik_ : https://docs.traefik.io/

En el caso de querer activar esta configuración se han configurar las
variables de entorno (XXX_DOMAIN) en el fichero `.env`.

### Variables de entorno
Definir el conjunto de variables de entorno expuestas en el fichero
[docker-compose.yml](docker-compose.yml)

#### Ejecutando docker-compose
Una forma de definir las variables de entorno de forma unificada es
declararlas en un fichero `.env` en el mismo directorio donde se encuentra
el fichero `docker-compose.yml`. Todas las variables de entorno que se
requieren pueden ser declaradas en ese fichero con el formato:

```shell
OPEN_SUBTITLES_USER=your-User
OPEN_SUBTITLES_PASS=your-passw0rd
...
```
#### Ejecutando docker stack deploy (swarm mode)
>Hacer deploy de un stack swarm requiere la activación de Swarm en el entorno docker (swarm mode).
En el caso de arrancar los contenedores como servicios de un __stack Swarm__, no
será posible que se lea directamente las variables de este fichero `.env`.
Se ha de cargar las variables manualmente. Por ejemplo:

```
source ./.env
```
> Importante: En este caso para poder ejectura `source` sobre el fichero `.env` este tiene
que incorporar por cada línea el comando `export`. Aquí un ejemplo de la lista
de variables de .env con el comando `export`.

```shell
# Datos de autenticación para la descarga de subtitulos vía
# https://www.opensubtitles.org/es
export OPEN_SUBTITLES_USER=your-User
export OPEN_SUBTITLES_PASS=your-passw0rd
export HTTP_PROXY=
#Your local network ip here
export HOST_IP=
export NO_PROXY=localhost
export RSS_FEED=http://showrss.info/user/21294.rss?magnets=true&namespaces=true&name=null&quality=null&re=null
#Rutas de volumenes
export DOWNLOADS_DIR=/media/storage/downloads/qbittorrent
export DOWNLOADS_COMPLETE=/media/storage/downloads/qbittorrent/complete
export VIDEOS_REPO_DIR=/media/storage/videos
export SERIES_REPO_DIR=/media/storage/videos/series
export MOVIE_REPO_DIR=/media/storage/videos/peliculas
export MUSIC_REPO_DIR=/media/storage/music
export PICTURE_REPO_DIR=/media/storage/pictures

# Traefik configuration - domains
#HEADPHONES_DOMAIN=jagmedia-headphones.duckdns.org
export RADARR_DOMAIN=xxxxx.duckdns.org
export SONARR_DOMAIN=xxxxx.duckdns.org
export LIDARR_DOMAIN=xxxxx.mooo.com
export JACKETT_DOMAIN=xxxx.mooo.com
export TRANSMISSION_DOMAIN=xxxxx.duckdns.org
export ORGANIZR_DOMAIN=xxxxx.duckdns.org
export FILEBROWSER_DOMAIN=xxxxx.mooo.com

# Process IDs
export PGID=1000
export PUID=1000
```


### Arranque de los servicios vía `docker-compose`

El arranque conjunto:

```shell

docker-compose up -d

```
### Arranque de los servicios vía `docker stack deploy`

```shell
docker stack deploy --compose-file docker-stack.yml my-stack
```
Parada:

```shell
docker stack rm my-stack
```

Listar servicios:

```shell
docker stack services
```

### Postproceso de subtitulos para series
El script [`series-post-process-subtitles.sh`](scripts/filebot-scripts/series-post-process-subtitles.sh) permite descargar subtitulos pendientes de bajo una carpeta concreta bajo la ubicacion de series.

Es posible ejecutarlo directamente desde el host via:

`docker exec -it filebot_sub ./videos-postprocess.sh "Game of Thrones"`

En este ejemplo se procesara la carpeta "Game of Thrones" baja la carpeta de series (definida en [docker-compose.yml](docker-compose.yml)) descargando todos los subtitulos que falten, siempre que esten disponibles.

Otra posibilidad es repasar los subtitulos de la lista completa de series:

`docker exec -it filebot_sub ./videos-postprocess.sh GLOBAL`

### Skip de torrents de la descarga actual
En el momento de descarga de torrents vía transmission-daemon podemos
consultar la lista de descarga actuales mediante:

```
> transmission-remote -l

ID     Done       Have  ETA           Up    Down  Ratio  Status       Name
  10    n/a       None  Unknown      0.0     0.0   None  Idle         Suits+S07E06+HDTV+x264+SVA
  11    n/a       None  Unknown      0.0     0.0   None  Idle         Dark+Matter+S03E12+HDTV+x264+SVA
Sum:              None               0.0     0.0
```
> Nota: Dado con los contenedores comparten al red con el host el
proceso que levanta el servicio de `filebot_sub` de transmission-daemon
es directamente visible desde el host con `transmission-remote -l`

Para ignorar el torrent identificado como **11**:

`docker exec -it filebot_sub ./skip_torrents.sh 11`

## Configuración como servicio _systemd_
Es posible configurar un conjunto de servicios docker-compose en forma
de _unit_ _systemd_.
> Ver:

> [Systemd Essentials: Working with Services, Units, and the Journal](https://www.digitalocean.com/community/tutorials/systemd-essentials-working-with-services-units-and-the-journal)

> [Cómo crear servicios en Ubuntu 16.04 con systemd](http://www.vozidea.com/como-crear-servicios-en-ubuntu-con-systemd)

> [Manage containers with Systemd](https://opsnotice.xyz/manage-containers-with-systemd/)

Crear el fichero que define el nuevo servicio o _unit_:

`/etc/systemd/system/plex-compose.service`:

```shell
[Unit]
Description=Plex Compose
After=NetworkManager-wait-online.service
Requires=NetworkManager-wait-online.service

[Service]
Type=simple
#Restart=always
ExecStartPre=-/bin/bash /home/juguerre/local-dev/repos/docker-transmission-filebot/service.sh stop
ExecStop=/bin/bash /home/juguerre/local-dev/repos/docker-transmission-filebot/service.sh stop
ExecStart=/bin/bash /home/juguerre/local-dev/repos/docker-transmission-filebot/service.sh start

[Install]
WantedBy=multi-user.target

```

En esta definición se invoca al fichero [service.sh](service.sh) que es
un _wrapper_ muy simple que sirve para ejecutar docker-compose desde
el directorio donde se puede hallar el fichero .env (opcional) que carga
 las variables de entorno para docker-compose.yml.

Ejecutar:

`sudo systemctl daemon-reload`

`sudo systemctl enable plex-compose.service`

Status:

`sudo systemctl status plex-compose.service`

Start:

`sudo systemctl start plex-compose.service`

Stop:

`sudo systemctl stop plex-compose.service`
