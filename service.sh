#!/usr/bin/env bash
# $1 debería contener start o stop

# Cambio a directorio donde se encuentra .env
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ${DIR}

case $1 in
start)
    docker-compose up
   ;;
stop)
    docker-compose stop
   ;;
esac