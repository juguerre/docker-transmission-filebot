#!/bin/bash
#
readonly SERVER_CONTAINER="plex"
function d_start ( ){
	echo  "${SERVER_CONTAINER} Container: starting service"
	docker start ${SERVER_CONTAINER}
	 sleep  5
	echo  " $(docker ps | grep ${SERVER_CONTAINER}) "
}

function d_stop ( ){
	echo  "${SERVER_CONTAINER} container: stopping container ( $(docker ps | grep ${SERVER_CONTAINER})  )"
	docker stop ${SERVER_CONTAINER}

 }

function d_status ( ){
	CONTAINER_ID=$(docker ps -a | grep -v Exit | grep ${SERVER_CONTAINER} | awk '{print $1}')
    if [[ -z ${CONTAINER_ID} ]] ; then
        echo "${SERVER_CONTAINER} Not running."
        return 1
    else
        echo "Running in container: $CONTAINER_ID"
        return 0
    fi

}

function usage (){
    local EXEC_PROGRAM=$0
    echo  "Usage: ${EXEC_PROGRAM} {start | stop | reload | status}"

}
function main (){

    local ACTION=$1
    case  "${ACTION}"  in
        start )
            d_start
            ;;
        stop )
            d_stop
            ;;
        reload )
            d_stop
            sleep  5
            d_start
            ;;
        status )
            d_status
            ;;
        * )
            usage
            exit  1
        ;;
    esac

    exit  0
}
# Management instructions of the service

main "$@"

