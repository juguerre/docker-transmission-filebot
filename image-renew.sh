#!/usr/bin/env bash

_usage_image_renew(){
    cat <<EOF

Usage:
        image-renew.sh <service-name> <image-name>
EOF
}

image-renew(){
    local service image
    service="${1}"
    image="${2}"

    docker-compose stop "${service}"
    docker-compose rm -f "${service}"
    docker rmi -f "${image}"
    docker-compose up -d "${service}"
}

_main(){

if [ -z $1 ];then
    #asume sourcing command
    return
elif [ -z $2 ];then
    _usage_image_renew
fi

image-renew "${1}" "${2}"

}

_main "$@"