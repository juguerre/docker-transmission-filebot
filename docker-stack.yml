version: '3.7'
services:
  plex:
      image: linuxserver/plex
      restart: unless-stopped
      environment:
        VERSION: latest
      deploy:
        resources:
          limits:
            memory: 500M

      volumes:
        - plex_data:/config
        - plex_data:/transcode
        - ${SERIES_REPO_DIR}:/data/tvshows
        - ${MOVIE_REPO_DIR}:/data/movies
        - ${MUSIC_REPO_DIR}:/data/music
        - ${PICTURE_REPO_DIR}:/data/pictures
      networks:
        - outside
  transmission:
        image: transmission:latest
        build: ./images/transmission
        depends_on:
          - traefik
        deploy:
          labels:
            - "traefik.backend=transmission"
            - "traefik.port=9091"
            - "traefik.docker.network=traefik-swarm"
            - "traefik.enable=true"
            - "traefik.frontend.rule=Host:${TRANSMISSION_DOMAIN}"

        volumes:
          - transmission-volume:/config
          - ${DOWNLOADS_DIR}:/downloads
          - ${DOWNLOADS_COMPLETE}:/downloads/complete

        environment:
          PGID: ${PGID}
          PUID: ${PUID}
          #Proxy configuration
          http_proxy: ${HTTP_PROXY}
          https_proxy: ${HTTP_PROXY}
          no_proxy: ${NO_PROXY}

        ports:
          - "9091:9091"

        networks:
          - traefik-swarm

  sonarr:
      image: linuxserver/sonarr
      depends_on:
        - traefik
        - transmission
      deploy:
        labels:
          - "traefik.port=8989"
          - "traefik.docker.network=traefik-swarm"
          - "traefik.enable=true"
          - "traefik.frontend.rule=Host:${SONARR_DOMAIN}"
      ports:
        - "8989:8989"
      environment:
        PGID: ${PGID}
        PUID: ${PUID}
      volumes:
        - ${SERIES_REPO_DIR}:/tv
        - ${DOWNLOADS_DIR}:/downloads
        - /etc/localtime:/etc/localtime:ro
        - sonarr-volume:/config
      networks:
        - traefik-swarm

  radarr:
      image: linuxserver/radarr
      depends_on:
        - traefik
        - transmission
      deploy:
        labels:
          - "traefik.port=7878"
          - "traefik.docker.network=traefik-swarm"
          - "traefik.enable=true"
          - "traefik.frontend.rule=Host:${RADARR_DOMAIN}"
      ports:
        - "7878:7878"
      environment:
        PGID: ${PGID}
        PUID: ${PUID}
      volumes:
        - ${MOVIE_REPO_DIR}:/movies
        - ${DOWNLOADS_DIR}:/downloads
        - /etc/localtime:/etc/localtime:ro
        - radarr-volume:/config
      networks:
        - traefik-swarm

  lidarr:
      image: linuxserver/lidarr
      deploy:
        labels:
          - "traefik.port=8686"
          - "traefik.docker.network=traefik-swarm"
          - "traefik.enable=true"
          - "traefik.frontend.rule=Host:${LIDARR_DOMAIN}"
      ports:
        - "8686:8686"
      environment:
        PGID: ${PGID}
        PUID: ${PUID}
        TZ: "Europe/Madrid"
      volumes:
        - ${MUSIC_REPO_DIR}:/music
        - ${DOWNLOADS_DIR}:/downloads
        - /etc/localtime:/etc/localtime:ro
        - lidarr-volume:/config
      networks:
        - traefik-swarm

  jackett:
      image: linuxserver/jackett
      depends_on:
        - traefik
      deploy:
        labels:
          - "traefik.port=9117"
          - "traefik.docker.network=traefik-swarm"
          - "traefik.enable=true"
          - "traefik.frontend.rule=Host:${JACKETT_DOMAIN}"
      ports:
        - "9117:9117"
      environment:
        PGID: ${PGID}
        PUID: ${PUID}
      volumes:
        - ${DOWNLOADS_DIR}:/donwloads
        - /etc/localtime:/etc/localtime:ro
        - jackett-volume:/config
      networks:
        - traefik-swarm

  organizr:
      image: organizrtools/organizr-v2
      depends_on:
        - traefik
      deploy:
        labels:
          - "traefik.port=80"
          - "traefik.docker.network=traefik-swarm"
          - "traefik.enable=true"
          - "traefik.frontend.rule=Host:${ORGANIZR_DOMAIN}"
      ports:
        - "8080:80"
      environment:
        PGID: ${PGID}
        PUID: ${PUID}
      volumes:
        - organizr-volume:/config
      networks:
        - traefik-swarm
  filebrowser:
      image: filebrowser/filebrowser
      depends_on:
        - traefik
      deploy:
        labels:
          - "traefik.port=80"
          - "traefik.docker.network=traefik-swarm"
          - "traefik.enable=true"
          - "traefik.frontend.rule=Host:${FILEBROWSER_DOMAIN}"
      ports:
        - "81:80"
      volumes:
        - /media/storage:/srv
        - plex_data:/srv/plex
        - $PWD/images/filebrowser/config.json:/config.json
        - $PWD/images/filebrowser/database.db:/etc/database.db
      networks:
        - traefik-swarm

  portainer:
      image: portainer/portainer
      restart: unless-stopped
      ports:
        - "9000:9000"
      volumes:
        - /var/run/docker.sock:/var/run/docker.sock
        - portainer-volume:/data

  traefik:
      image: traefik:1.7.12 # The official Traefik docker image
      deploy:
        placement:
          constraints: [node.role == manager]
      ports:
        - "80:80"     # The HTTP port
        - "443:443"
        - "8081:8080" # The Web UI (enabled by --api)
      volumes:
        - /var/run/docker.sock:/var/run/docker.sock # So that Traefik can listen to the Docker events
        - $PWD/traefik-swarm.toml:/traefik.toml
        - $PWD/acme.json:/acme.json
      networks:
        - traefik-swarm

# Mapping local volumes to "actual" external docker-compose volumes
# Without this mapping, new swarm services volumes would  be created
# from scratch (except for plex_data)
volumes:
  transmission-volume:
    external: true
    name: "docker-transmission-filebot_transmission-volume"  
  sonarr-volume:
    external: true
    name: "dockertransmissionfilebot_sonarr-volume"
  radarr-volume:
    external: true
    name: "dockertransmissionfilebot_radarr-volume"
  lidarr-volume:
    external: true
    name: "docker-transmission-filebot_lidarr-volume"
  jackett-volume:
    external: true
    name: "dockertransmissionfilebot_jacket-volume"
  organizr-volume:
    external: true
    name: "dockertransmissionfilebot_organizr-volume"
  portainer-volume:
    external: true
    name: "dockertransmissionfilebot_portainer-volume"
  plex_data:
      external: true

networks:
  outside:
    external:
      name: "host"
  traefik-swarm:
    external:
      name: "traefik-swarm"
